
package ca.sheridancollege.trejo;

import java.util.ArrayList;
import java.util.List;

/**
 * A class that is meant to be used for practicing the skills necessary to
 * branch/merge and pull/push to a Git repository. For ICE 4, please add one
 * class that is named after your Capstone Group and add one instance of that
 * class to my ArrayList. If you wish to override the toString method in your
 * new class, you may.
 * 
 * @author Liz Dancy, 2017.
 * @author Ramses Trejo, 2020
 *
 */
public class ClassList {

	public static void main(String[] args) {
		List<Person> classList = new ArrayList<Person>();

		classList.add(new Person("Instructor", "Liz Dancy"));
		classList.add(new Person("Instructor", "Ramses Trejo"));
		classList.add(new Student("Student", "Austin Maggs"));
		classList.add(new Eman("Student", "Eman Butt"));
		classList.add(new Student("Student", "Vraj Bhagat"));

		classList.add(new Person("Assistant", "Kevin"));
		classList.add(new Person("Student", "Arashdeep Wander"));
		classList.add(new Person("Student", "Jake Arthurs"));
		classList.add(new Student("Student", "Mark Ryder"));
		classList.add(new Student("Student", "Gabby Diaz"));
		classList.add(new Student("Student", "Daniel Patawaran"));
		classList.add(new Student("Student", "Mansi Mansi"));
		classList.add(new MyClass("Student", "Nathan Lapp"));
		classList.add(new Student("Student", "Maria Mathew"));
		classList.add(new Student("Student", "Tejas Patel"));
		classList.add(new Student("Student", "Malek Gazem"));
		classList.add(new Student("Student", "Xuanchen Liu"));
		classList.add(new Student("Student", "Eman Butt"));
		classList.add(new Student("Student", "Dhruvi Patel"));
		classList.add(new Student("Student", "Syed Kabir"));
		classList.add(new Student("Student", "Marium Rameez"));
		classList.add(new Student("Student", "Zoe Wang"));
		classList.add(new Student("Student", "Harleen Kaur"));
		classList.add(new Student("Student", "Yashkumar Barot"));
		classList.add(new Student("Student", "Harjinder Cheema"));
		classList.add(new Student("Student", "Taranpreet Singh"));
		classList.add(new Student("Student","Mohammad Seena Akbar"));
		classList.add(new Student("Student","Sarvesh Badhwar"));
		classList.add(new Student("Student","Michael Kornecki"));
		classList.add(new Lab3Class("Student", "Nathaniel Kawal"));
		classList.add(new Lab3Class("Student", "Garrett Hofland"));
		classList.add(new Lab3Class("Student", "Baran Cetin"));
		

		System.out.println(classList);

	}

}
